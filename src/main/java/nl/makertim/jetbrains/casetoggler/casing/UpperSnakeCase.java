package nl.makertim.jetbrains.casetoggler.casing;

public class UpperSnakeCase extends CharSeparatedCase {

	public UpperSnakeCase() {
		super("_", "-");
	}

	@Override
	public boolean matches(String input) {
		return input.toUpperCase().equals(input)
				&& input.contains(separator);
	}

	@Override
	public String transform(String[] input) {
		return super.transform(input).toUpperCase();
	}
}
