package nl.makertim.jetbrains.casetoggler.casing;

public interface Casing {

	Casing[] CASINGS_ORDER = new Casing[] {
			new CamelCase(),
			new PascalCase(),
			new SnakeCase(),
			new KebabCase(),
			new UpperSnakeCase(),
			new UpperKebabCase(),
	};

	String[] separate(String input);

	boolean matches(String input);

	String transform(String[] input);

	static String transformToNextCasing(String input) {
		input = input.trim();

		int currentCasingIndex = -1;
		for (int i = 0; i < CASINGS_ORDER.length; i++) {
			if (CASINGS_ORDER[i].matches(input)) {
				currentCasingIndex = i;
				break;
			}
		}

		if (currentCasingIndex == -1) {
			return input;
		}

		String[] split = CASINGS_ORDER[currentCasingIndex].separate(input);
		return CASINGS_ORDER[(currentCasingIndex + 1) % CASINGS_ORDER.length].transform(split);
	}
}
