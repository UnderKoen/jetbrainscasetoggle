package nl.makertim.jetbrains.casetoggler.casing;

public class CharSeparatedCase implements Casing {

	protected final String separator;
	protected final String[] nonAllowedCharacters;

	public CharSeparatedCase(String separator, String... nonAllowedCharacters) {
		this.separator = separator;
		this.nonAllowedCharacters = nonAllowedCharacters;
	}

	@Override
	public String[] separate(String input) {
		return input.split(separator);
	}

	@Override
	public boolean matches(String input) {
		for (String nonAllowedCharacter : nonAllowedCharacters) {
			if (input.contains(nonAllowedCharacter)) {
				return false;
			}
		}
		return input.toLowerCase().equals(input)
				&& input.contains(separator);
	}

	@Override
	public String transform(String[] input) {
		return String.join(separator, input).toLowerCase();
	}
}
