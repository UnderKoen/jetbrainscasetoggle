package nl.makertim.jetbrains.casetoggler;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import nl.makertim.jetbrains.casetoggler.casing.Casing;
import org.jetbrains.annotations.NotNull;

public class ToggleCase extends AnAction {

	@Override
	public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
		Project project = anActionEvent.getProject();
		Editor editor = anActionEvent.getData(CommonDataKeys.EDITOR);

		if (project == null || editor == null) {
			return;
		}

		Document document = editor.getDocument();
		String selectedString = editor.getSelectionModel().getSelectedText();
		if (selectedString == null || selectedString.isEmpty()) {
			editor.getSelectionModel().selectWordAtCaret(false);
			selectedString = editor.getSelectionModel().getSelectedText();
			if (selectedString == null || selectedString.isEmpty()) {
				return;
			}
		}

		String newString = Casing.transformToNextCasing(selectedString);
		int startSelection = editor.getSelectionModel().getSelectionStart();
		int endSelection = editor.getSelectionModel().getSelectionEnd();

		WriteCommandAction.runWriteCommandAction(project,
				() -> document.replaceString(startSelection, endSelection, newString)
		);
	}
}
