package nl.makertim.jetbrains.casetoggler;

import nl.makertim.jetbrains.casetoggler.casing.CamelCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CamelCaseTest {

	private final CamelCase casing = new CamelCase();

	@Test
	public void testCamelCaseTransform() {
		String[] input = { "test", "camel", "case" };

		String output = casing.transform(input);

		Assertions.assertEquals("testCamelCase", output);
	}

	@Test
	public void testCamelCaseTransform2() {
		String[] input = { "TEST", "camel", "case" };

		String output = casing.transform(input);

		Assertions.assertEquals("testCamelCase", output);
	}

	@Test
	public void testCamelCaseTransform3() {
		String[] input = { "TEST", "CAMEL", "CASE" };

		String output = casing.transform(input);

		Assertions.assertEquals("testCamelCase", output);
	}

	@Test
	public void testCamelCaseSeparate() {
		String input = "testCamelCase";

		String[] output = casing.separate(input);

		Assertions.assertArrayEquals(new String[] { "test", "Camel", "Case" }, output);
	}

	@Test
	public void testCamelCaseSeparate2() {
		String input = "TestCamelCase";

		String[] output = casing.separate(input);

		Assertions.assertArrayEquals(new String[] { "Test", "Camel", "Case" }, output);
	}

	@Test
	public void testCamelCaseSeparate3() {
		String input = "SQLBattle";

		String[] output = casing.separate(input);

		Assertions.assertArrayEquals(new String[] { "SQL", "Battle" }, output);
	}

	@Test
	public void testCamelCaseSeparate4() {
		String input = "sQLBattle";

		String[] output = casing.separate(input);

		Assertions.assertArrayEquals(new String[] { "s", "QL", "Battle" }, output);
	}
}
